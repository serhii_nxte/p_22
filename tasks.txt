SKILLS
-----
- Using Rules

MODULES
-----
- Rules

HOOKS
-----
- hook_rules_event_info()
- hook_rules_condition_info()
- hook_rules_action_info()

STEPS
-----	
- Read about the Rules module
- Use the Rules UI to create a Rule that:
	- Acts on a Node being updated
	- Sends an e-mail to your e-mail address
	- Node title as subject of e-mail
	- Node text as message of e-mail
- Create your own set of Event / Condition / Action with the right hooks
	- Use this to configure a custom Rule using the Rules UI, like
		- Increase a saved variable by +1 (Action) every time a user with a username of 6 characters (Condition) clears the cache (Event)
		- Send an e-mail (Action) every time module X (Condition) is enabled (Event)